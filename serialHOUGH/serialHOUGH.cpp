#include "stdafx.h"
#include <iostream>
#include <cmath>
#include <random>
#include <time.h>
#include <string>
#include <stdlib.h>
#include <chrono>

#define PI 3.14159265

using namespace std;
using namespace System;
using namespace System::Drawing;


int main()
{
	//wczytanie �cie�ek z konsoli
	cout << "Wprowadz sciezke do pliku:\n";
	string input;
	cin >> input;
	String^ input_path = gcnew String(input.c_str());
	cout << "Wprowadz sciezke do zapisu (wraz z nazwa pliku):\n";
	string output;
	cin >> output;
	String^ output_path = gcnew String(output.c_str());


	//alokacja obrazka wej�ciowego
	Bitmap^ picture = gcnew Bitmap(input_path, true);
	int height = picture->Height;
	int width = picture->Width;


	//alokacja tablicy g�os�w pixeli 
	int acc_height = sqrt(width*width + height*height) + 1;
	int acc_width = 360;

	double center_x = width / 2;	//po�owa szeroko�ci obrazka
	double center_y = height / 2;	//po�owa wysoko�ci obrazka


	//alokacja tablicy g�os�w i bitmapy na output
	int* accumulator = (int*)calloc(acc_height * acc_width, sizeof(int));
	Bitmap^ result = gcnew Bitmap(acc_width, acc_height);

	double max = 0;					//maksimum do normalizacji


	//transformacja HOUGH
	//dla kazdego pixela w obrazie jest wyliczany 'glos' i dodawany do tabeli (accumulator)
	//rho = x * cos(theta) + y * sin(theta);	//rho mo�e wyj�� ujemne!
	//H(rho, theta)++;

	auto start_time = chrono::high_resolution_clock::now();

	for (int idx = 0; idx < height*width; idx++)
	{
		int row = idx / width;
		int col = idx % width;

		int value = picture->GetPixel(col, row).R;
		if (value == 0) continue;

		for (int theta = 0; theta < 360; theta++)
		{
			double radians = theta * PI / 180;
			double r = ((double)col - center_x - 1) * cos(radians) + ((double)row - center_y - 1) * sin(radians);
			int ro = (int)(r + acc_height / 2 + 1);
			accumulator[ro*acc_width + theta]++;

			if (accumulator[ro*acc_width + theta] > max)		//szukanie maxa do normalizacji
				max = accumulator[ro*acc_width + theta];
		}
	}


	auto end_time = chrono::high_resolution_clock::now();
	auto milliseconds = (end_time - start_time) / chrono::milliseconds(1);
	cout << "Transformacja Hough'a zajela " << milliseconds << " milisekund.\n";

	//normalizacja do zakresu 0-255
	for (int idx = 0; idx < acc_height*acc_width; idx++) 
	{
			accumulator[idx] = (int)(accumulator[idx] * 255.0 / max);
	}

	//zapisanie do bitmapy i pliku
	for (int idx = 0; idx < acc_height*acc_width; idx++) 
	{
			result->SetPixel(idx%acc_width, (int)idx/acc_width, Color::FromArgb(accumulator[idx], accumulator[idx], accumulator[idx]));
	}
	result->Save(output_path);

	return 0;
}

